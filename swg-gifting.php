<?php
/**
 * Plugin Name:     SWG Gifting
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          YOUR NAME HERE
 * Author URI:      YOUR SITE HERE
 * Text Domain:     swg-gifting
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Swg_Gifting
 */

function swg_is_giftable( $product) {
  $giftable_product_slugs = array( 'gift-digital-membership', 'gift-print-magazine-subscription', 'gift-combo-digital-membership-plus-print-subscription' );
  if( $product)
    return( in_array( $product->get_slug() , $giftable_product_slugs ));
  else {
    return false;
  }
}

function swg_is_shippable_gift( $product) {
  $giftable_product_slugs_need_shipping = array( 'gift-print-magazine-subscription', 'gift-combo-digital-membership-plus-print-subscription' );
  if( $product)
    return( in_array( $product->get_slug() , $giftable_product_slugs_need_shipping ));
  else {
    return false;

  }
}

function swg_cart_has_a_gift()  {

 	$found = false;
 	$cart = WC()->cart;
  if ( $cart) {

   	$size = sizeof( $cart->get_cart() );
   	//check if product already in cart
   	if ( $size > 0 ) {
   		foreach ( $cart->get_cart() as $cart_item_key => $values ) {
   			//var_dump( $values);
        $product = wc_get_product( $values['product_id'] );
        $found = swg_is_giftable( $product );

   			if ( $found)
   				break;
   		}
    }
  }


  return( $found);

}


 /**
  * Plugin Name: WooCommerce Subscriptions Restrict Gifting to Products
  * Description: Restrict the option to gift to specific products.
  * Author: James Allan
  * Version: 1.0
  * License: GPL v2
  */

 function swg_byo_wcsrgp_is_giftable_product( $is_giftable, $product ) {
   if ( !is_admin() ) {
     return swg_is_giftable( $product);
   }
   return ( $is_giftable );

 }

add_filter( 'wcsg_is_giftable_product', 'swg_byo_wcsrgp_is_giftable_product', 10, 2 );


add_filter( 'woocommerce_cart_needs_shipping', 'swg_gift_products_need_shipping', 10 , 1);
function swg_gift_products_need_shipping( $needs_shipping) {
 	global $needs_shipping_product_ids;
  if ( is_admin() )
    return( $needs_shipping);

 	//$needs_shipping = false;
 	$found = false;
 	$cart = WC()->cart;
  if ( $cart ) {

   	$size = sizeof( $cart->get_cart() );
   	//check if product already in cart
   	if ( $size > 0 ) {
   		foreach ( $cart->get_cart() as $cart_item_key => $values ) {
   			//var_dump( $values);
        $product = wc_get_product( $values['product_id'] );
        $found = swg_is_shippable_gift( $product );

   			if ( $found)
   				break;

   		}
      if ( $found ) {
        $needs_shipping = true;
      }
   	}
  }


 	//$needs_shipping = $found;

 	return( $needs_shipping);
 }


// change checkout language for gifts
add_filter('gettext', 'swg_gift_checkout_custom_renaming', 10, 3);
function swg_gift_checkout_custom_renaming( $replaced_text, $source_text, $domain ) {

       // Set here the complete description to be replaced
  if( $source_text == 'Ship to a different address?' ) {
       // check for gifting product
       $found = swg_cart_has_a_gift();

        // Set here your replacement description
        if ( $found )
          $replaced_text = __( 'Enter gift recipient address.',$domain );
 	 }
   return $replaced_text;
 }

/*
 * Change button for gifts
 */
//remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
add_filter( 'woocommerce_loop_add_to_cart_link', 'swg_gifting_change_add_to_cart', 10, 3);
function swg_gifting_change_add_to_cart( $link, $product, $args ) {
  global $giftable_product_ids;

  if ( swg_is_giftable( $product ) ) {
    // change button destination, and text
    $link = 	sprintf( '<a href="%s" class="%s" >%s</a>',
    		esc_url( get_permalink( $product->get_id()) ),
    		esc_attr( isset( $args['class'] ) ? $args['class'] : 'button' ),
    		esc_html( 'View Product' ) );

  }

  return $link;
}

// make email field visible
add_filter( 'wcsg_recipient_email_field_args', 'swg_filter_recipient_email_field_args', 10, 2);
function swg_filter_recipient_email_field_args( $email_field_args, $email ) {

  array_pop( $email_field_args['style_attributes']);
  array_push( $email_field_args['style_attributes'], 'display: visible' );
  //$email_field_args['required'] = array();
  //array_push( $email_field_args['required'], 'true' );

  return( $email_field_args );
}

// add javascript to make recipient email required
function swg_gifting_scripts () {
  wp_enqueue_script( 'swg-gifting-js', plugin_dir_url( __FILE__ ) . 'js/swg-gifting.js', array('jquery'), '', true);
}
add_action( 'wp_enqueue_scripts', 'swg_gifting_scripts' );


// fix coupon
$swg_allow_gift_coupons = false;
//
add_action( 'woocommerce_before_template_part', 'swg_gifting_coupon_fix', 999, 1 );
function swg_gifting_coupon_fix( $template_name ) {
  global $swg_allow_gift_coupons;
  $swg_allow_gift_coupons = swg_cart_has_a_gift();
}


// second fix coupon (does not show coupon math in checkout)
$swg_gifting_save_cart_coupons = null;

//add_action( 'woocommerce_before_template_part', 'swg_gifting_coupon_fix1', 10, 1 );
function swg_gifting_coupon_fix1( $template_name ) {
  global $swg_allow_gift_coupons;
  global $swg_gifting_save_cart_coupons;

  $swg_allow_gift_coupons = swg_cart_has_a_gift();
  if ( $swg_allow_gift_coupons) {

    $cart = WC()->cart;
    if ( $cart) {
      $swg_gifting_save_cart_coupons = $cart->applied_coupons;
      WC()->cart->applied_coupons = array();
    }
  }
}


//add_action( 'woocommerce_after_template_part', 'swg_gifting_coupon_fix2', 10, 1 );
function swg_gifting_coupon_fix2( $template_name ) {
  global $swg_allow_gift_coupons;
  global $swg_gifting_save_cart_coupons;

  $swg_allow_gift_coupons = swg_cart_has_a_gift();
  if ( $swg_allow_gift_coupons) {

    $cart = WC()->cart;
    if ( $cart) {
      $swg_gifting_save_cart_coupons = $cart->applied_coupons;
      if ( $swg_gifting_save_cart_coupons )
        WC()->cart->applied_coupons = $swg_gifting_save_cart_coupons;
    }
  }
}
